VERSION = 0.2.2
ANDROID_VERSION_CODE = 4

DEFINES += QT_DEPRECATED_WARNINGS
unix: QMAKE_CXXFLAGS += -Wextra -Werror=format-security -D_FORTIFY_SECURITY=2

CONFIG(release, debug|release): DEFINES += QT_NO_DEBUG_OUTPUT

CONFIG(debug, debug|release) {
	CONFIG += qml_debug declaratice_debug warn_on
	unix: QMAKE_CXXFLAGS += -Werror
}

CONFIG(debug, debug|release): NAMEPOSTFIX="_debug"
CONFIG(release, debug|release): NAMEPOSTFIX=""

MOC_DIR = moc
OBJECTS_DIR = obj

android {
	QMAKE_CXXFLAGS += -std=c++11
}

unix:!android {
	CONFIG += link_pkgconfig
	PKGCONFIG += libsodium
}

win32 {
	#TODO: use pkgconfig
	LIBS += -L$$PWD/libsodium-win64/bin -lsodium-23
	INCLUDEPATH += $$PWD/libsodium-win64/include
	CONFIG(debug, debug|release): CONFIG += console
}

android {
	QT += androidextras
	ANDROID_PACKAGE_SOURCE_DIR=$$PWD/android-sources
	INCLUDEPATH += $$(OPENRECIPES_ANDROID_SYSROOT)/include
	LIBS += -L$$(OPENRECIPES_ANDROID_SYSROOT)/lib -lsodium
	ANDROID_EXTRA_LIBS += $$(OPENRECIPES_ANDROID_SYSROOT)/lib/libsodium.so
}

doxygen.commands = doxygen $$PWD/Doxyfile
QMAKE_EXTRA_TARGETS += doxygen
