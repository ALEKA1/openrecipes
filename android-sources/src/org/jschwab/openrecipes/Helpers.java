package org.jschwab.openrecipes;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Base64;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.lang.StringBuilder;
import java.lang.IllegalArgumentException;
import java.io.InputStreamReader;

public class Helpers {
	public static String readFile(Uri uri, Context context) throws FileNotFoundException, IOException {
		InputStream stream = context.getContentResolver().openInputStream(uri);
		BufferedReader r = new BufferedReader(new InputStreamReader(stream));
		StringBuilder sb = new StringBuilder();
		String line;
		while ((line = r.readLine()) != null) {
			sb.append(line).append('\n');
		}
		stream.close();
		return sb.toString();
	}

	public static String readImage(Uri uri, Context context) {
		try {
			InputStream stream = context.getContentResolver().openInputStream(uri);
			Bitmap bitmap = BitmapFactory.decodeStream(stream);
			int newWidth = (int) ((double)bitmap.getWidth() * 120. / (double)bitmap.getHeight());
			try {
				bitmap = Bitmap.createScaledBitmap(bitmap, newWidth, 120, false);
			} catch (IllegalArgumentException e) {
				return "";
			}
			stream.close();
			ByteArrayOutputStream data = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, data);
			String begin = "data:image/png;base64,";
			return begin + Base64.encodeToString(data.toByteArray(), Base64.DEFAULT);
		} catch (FileNotFoundException e) {
			return "";
		} catch (IOException e) {
			return "";
		}
	}

	public static native boolean importRecipe(String recipeXml);

	public static native String getRecipeXml(String recipeIdHex);

	public static native String getRecipeName(String recipeIdHex);

	static {
		System.loadLibrary("openrecipes");
	}
}
