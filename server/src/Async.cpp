/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Async.h"
#include "TcpConnection.h"
#include "BackendWorker.h"
#include "DbItem.h"

#include <vector>

void Async::getIds(TcpConnection *con, quint64 userId) {
	Async *a = new Async;
	connect(a, &Async::gotIds, con, &TcpConnection::onGotIds);
	BackendWorker::appendTask([a, userId](BackendWorker *w) -> void {
			GetIdsRes res = BackendWorker::getIds(userId);
			connect(w, &BackendWorker::gotIds, a, &Async::onGotIds);
			emit w->gotIds(res);
	});
}

void Async::addOrUpdate(TcpConnection *con, const QByteArray &id, const QByteArray &cipher, const QByteArray &nonce, quint64 userId) {
	Async *a = new Async;
	connect(a, &Async::addedOrUpdated, con, &TcpConnection::onAddedOrUpdated);
	BackendWorker::appendTask([a, id, cipher, nonce, userId](BackendWorker *w) -> void {
			bool succ = BackendWorker::addOrUpdate(id, cipher, nonce, userId);
			connect(w, &BackendWorker::addedOrUpdated, a, &Async::onAddedOrUpdated);
			emit w->addedOrUpdated(succ);
	});
}

void Async::deleteEntry(TcpConnection *con, const QByteArray &id, quint64 userId) {
	Async *a = new Async;
	connect(a, &Async::deleted, con, &TcpConnection::onDeleted);
	BackendWorker::appendTask([a, id, userId](BackendWorker *w) -> void {
			bool succ = BackendWorker::deleteEntry(id, userId);
			connect(w, &BackendWorker::deleted, a, &Async::onDeleted);
			emit w->deleted(succ);
	});
}

void Async::getChanged(TcpConnection *con, quint64 timestamp, quint64 userId) {
	Async *a = new Async;
	connect(a, &Async::gotChanged, con, &TcpConnection::onGotChanged);
	BackendWorker::appendTask([a, timestamp, userId](BackendWorker *w) -> void {
			GetChangedRes res = BackendWorker::getChanged(timestamp, userId);
			connect(w, &BackendWorker::gotChanged, a, &Async::onGotChanged);
			emit w->gotChanged(res);
	});
}

void Async::getSyncKey(TcpConnection *con, const QByteArray &id) {
	Async *a = new Async;
	connect(a, &Async::gotSyncKey, con, &TcpConnection::onGotSyncKey);
	BackendWorker::appendTask([a, id](BackendWorker *w) -> void {
			GetSyncKeyRes res = BackendWorker::getSyncKey(id);
			connect(w, &BackendWorker::gotSyncKey, a, &Async::onGotSyncKey);
			emit w->gotSyncKey(res);
	});
}

void Async::setSyncKey(TcpConnection *con, const QByteArray &id, const QByteArray &cipher, const QByteArray &nonce) {
	Async *a = new Async;
	connect(a, &Async::settedSyncKey, con, &TcpConnection::onSettedSyncKey);
	BackendWorker::appendTask([a, id, cipher, nonce](BackendWorker *w) -> void {
			bool succ = BackendWorker::setSyncKey(id, cipher, nonce);
			connect(w, &BackendWorker::settedSyncKey, a, &Async::onSettedSyncKey);
			emit w->settedSyncKey(succ);
	});
}

void Async::onGotIds(const GetIdsRes &res) {
	emit gotIds(res);
	deleteLater();
}

void Async::onAddedOrUpdated(bool succ) {
	emit addedOrUpdated(succ);
	deleteLater();
}

void Async::onDeleted(bool succ) {
	emit deleted(succ);
	deleteLater();
}

void Async::onGotChanged(const GetChangedRes &res) {
	emit gotChanged(res);
	deleteLater();
}

void Async::onSettedSyncKey(bool succ) {
	emit settedSyncKey(succ);
	deleteLater();
}

void Async::onGotSyncKey(const GetSyncKeyRes &res) {
	emit gotSyncKey(res);
	deleteLater();
}
