/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef HELPER_DEFS
#define HELPER_DEFS

#include "DbItem.h"

#include <utility>
#include <QByteArray>
#include <vector>

using GetIdsRes = std::pair<bool, std::vector<QByteArray>>;
using GetChangedRes = std::pair<bool, std::vector<DbItem>>;
using GetSyncKeyRes = std::pair<bool, std::pair<QByteArray, QByteArray>>;

#endif //HELPER_DEFS
