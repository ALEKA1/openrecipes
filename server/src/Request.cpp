/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Request.h"
#include "InternalError.h"

#include <QtEndian>
#include <cassert>

std::shared_ptr<Request> Request::getRequest(const QByteArray &keyword) {
	assert(keyword.size() == keywordSize);
	if (keyword == "GETPV") return std::make_shared<RequestGetProtocolVersion>();
	if (keyword == "GETST") return std::make_shared<RequestGetStartupTime>();
	if (keyword == "GETID") return std::make_shared<RequestGetIds>();
	if (keyword == "ADDUP") return std::make_shared<RequestAddOrUpdate>();
	if (keyword == "DELET") return std::make_shared<RequestDelete>();
	if (keyword == "GETCH") return std::make_shared<RequestGetChanged>();
	if (keyword == "CLOSE") return std::make_shared<RequestClose>();
	if (keyword == "ADDSK") return std::make_shared<RequestSetSyncKey>();
	if (keyword == "GETSK") return std::make_shared<RequestGetSyncKey>();
	throw IERROR("Invalid keyword");
}

Request& Request::operator+=(const QByteArray &data) {
	assert(static_cast<quint64>(data.size()) <= bytesNeeded());
	this->data += data;
	return *this;
};

quint64 RequestWithData::bytesNeeded() const {
	if (data.size() < 8) {
		return (8 - data.size());
	} else {
		const quint64 dataSize = qFromBigEndian<quint64>(data.constData());

		return dataSize - data.size() + 8;
	}
}

quint64 RequestWithoutData::bytesNeeded() const {
	return 0;
}

std::pair<QByteArray, QByteArray> RequestWithTwoParams::getParams(unsigned char offset) const {
	if (data.size() < 24 + offset) throw IERROR("Not enough data");
	const quint64 firstSize = qFromBigEndian<quint64>(data.data() + 8 + offset);
	const quint64 secondSize = qFromBigEndian<quint64>(data.data() + 16 + offset);
	if (static_cast<quint64>(data.size()) != 24 + firstSize + secondSize + offset) throw IERROR("Not enough data");

	const QByteArray first(data.data() + 24 + offset, firstSize);
	const QByteArray second(data.data() + 24 + firstSize + offset, secondSize);
	return std::make_pair(first, second);
}

Request::Type RequestGetProtocolVersion::getType() const {
	return Type::GetProtocolVersion;
}

Request::Type RequestGetStartupTime::getType() const {
	return Type::GetStartupTime;
}

Request::Type RequestGetIds::getType() const {
	return Type::GetIds;
}
	
Request::Type RequestAddOrUpdate::getType() const {
	return Type::AddOrUpdate;
}

QByteArray RequestIdCipherNonce::getId() const {
	if (data.size() < 24) throw IERROR("Data to short");
	return QByteArray(data.data() + 8, 16);
}

RequestIdCipherNonce::CipherAndNonce RequestIdCipherNonce::getCipherAndNonce() const {
	const std::pair<QByteArray, QByteArray> params = getParams(16);
	return CipherAndNonce(params.first, params.second);
}
	
Request::Type RequestDelete::getType() const {
	return Type::Delete;
}

QByteArray RequestDelete::getId() const {
	if (data.size() != 24) throw IERROR("Data has invalid length");
	return QByteArray(data.data() + 8, 16);
}
	
Request::Type RequestGetChanged::getType() const {
	return Type::GetChanged;
}

quint64 RequestGetChanged::getTimestamp() const {
	if (data.size() != 16) throw IERROR("Data has invalid length");
	return qFromBigEndian<quint64>(data.data() + 8);
}
	
Request::Type RequestClose::getType() const {
	return Type::Close;
}

Request::Type RequestGetSyncKey::getType() const {
	return Type::GetSyncKey;
}

QByteArray RequestGetSyncKey::getId() const {
	if (data.size() != 24) throw IERROR("Data has invalid length");
	return QByteArray(data.data() + 8, 16);
}

Request::Type RequestSetSyncKey::getType() const {
	return Type::SetSyncKey;
}
