TEMPLATE = subdirs

SUBDIRS = lib client
client.depends = lib

unix:!android {
	SUBDIRS += server windowsupdatesigner
	server.depends = lib
	windowsupdatesigner.depends = lib
}

!android {
	SUBDIRS += windowsupdatesigner
	windowsupdatesigner.depends = lib
}
