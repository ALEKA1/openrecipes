/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import QtQuick.Controls 2.0
import org.jschwab.recipes 1.0

ApplicationWindow {
	id: mainWindow

	width: 800
	height: 600
	visible: true
	title: Qt.application.name

	MediumLabel {
		width: Math.min(parent.width, implicitWidth)
		anchors.margins: 5
		anchors.centerIn: parent
		text: qsTr("Your version of Qt is not supported. Please update to Qt %1 or higher and try again.").arg(Globals.minimumQtVersion)
		horizontalAlignment: Text.AlignHCenter
		wrapMode: Text.Wrap
	}
}
