/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Window 2.2
import QtQuick.Layouts 1.3
import org.jschwab.recipes 1.0

ApplicationWindow {
	id: mainWindow

	width: 800
	height: 600
	visible: true
	title: Qt.application.name

	ModalDialog {
		id: updateDialog

		property string version: "0.0.0"

		standardButtons: Dialog.Yes | Dialog.No

		title: qsTr("Update available")
		ColumnLayout {
			anchors.fill: parent

			Label {
				Layout.maximumWidth: mainWindow.width - 30
				Layout.alignment: Qt.AlignHCenter
				text: qsTr("An update to version %1 is available. Do you wan't to install it and restart OpenRecipes? (Please make shure to save any unsaved changes beforhand.)").arg(updateDialog.version);
				wrapMode: Text.Wrap
			}
		}

		onAccepted: {
			updateDownloadDialog.open();
			backend.update(version)
		}
	}

	ModalDialog {
		id: updateDownloadDialog
		standardButtons: Dialog.NoButton

		title: qsTr("Downloading update…")

		ColumnLayout {
			anchors.fill: parent

			ProgressBar {
				Layout.alignment: Qt.AlignHCenter
				indeterminate: true
			}

			Label {
				Layout.maximumWidth: mainWindow.width - 30
				Layout.alignment: Qt.AlignHCenter
				text: qsTr("Please wait while the update is downloaded.")
				wrapMode: Text.Wrap
			}
		}

		Connections {
			target: backend
			onUpdateDownloadFailed: {
				updateDownloadFailedDialog.open();
				updateDownloadDialog.accept();
			}
		}
	}

	ModalDialog {
		id: updateDownloadFailedDialog
		standardButtons: Dialog.Ok

		title: qsTr("Update failed")

		ColumnLayout {
			anchors.fill: parent

			Label {
				Layout.maximumWidth: mainWindow.width - 30
				Layout.alignment: Qt.AlignHCenter
				text: qsTr("The download of the update failed.")
				wrapMode: Text.Wrap
			}
		}
	}

	StackView {
		id: stackView
		anchors.fill: parent
		initialItem: Item {
			id: mainRowLayout

			RecipesListView {
				id: recipesListView
				anchors.left: parent.left
				anchors.top: parent.top
				anchors.bottom: parent.bottom
				width: 200
			}

			RecipeView {
				id: recipeView
				anchors.left: recipesListView.right
				anchors.top: parent.top
				anchors.bottom: parent.bottom
				anchors.right: parent.right
			}

			function selectRecipe(recipe) {
				recipeView.recipe = recipe;
			}
		}
		
		function editRecipe(recipe) {
			push("qrc:/qml/RecipeEditView.qml", {"recipe": recipe});
		}

		function searchRecipes() {
			push("qrc:/qml/RecipesSearchView.qml");
		}

		function toSettings() {
			push("qrc:/qml/SettingsView.qml");
		}
	}

	Connections {
		target: backend
		onRecipeImported: {
			mainRowLayout.selectRecipe(backend.getRecipe(id));
			recipesListView.highlightRecipe(id);
		}
		onUpdateAvailable: function(version) {
			updateDialog.version = version;
			updateDialog.open();
		}
	}
}
