/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WINDOWS_SELF_UPDATE_H
#define WINDOWS_SELF_UPDATE_H

#include <QObject>
#include <QThread>
#include <QVersionNumber>

class QNetworkReply;

class WindowsSelfUpdate {
	private:
		QNetworkReply* getReply(const QString &url);

	protected:
		static QString getLocalMsiFileName();
		void getFile(const QString &url, const QString &saveTo);
		QByteArray getFileContent(const QString &url);

	public:
		static void cleanup();
};

class CheckForUpdateAsync : public QThread, public WindowsSelfUpdate {
	Q_OBJECT

	public:
		CheckForUpdateAsync(QObject *parent);
		void run() override;

	signals:
		void updateAvailable(const QVersionNumber &version);
		void noUpdateAvailable(bool checkSucceeded);
};

class UpdateAsync : public QThread, public WindowsSelfUpdate {
	Q_OBJECT

	private:
		const QVersionNumber version;

	public:
		UpdateAsync(QObject *parent, const QVersionNumber &version);
		void run() override;

	signals:
		void downloadFailed();
};

#endif //WINDOWS_SELF_UPDATE_H
