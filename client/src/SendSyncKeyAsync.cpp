/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "SendSyncKeyAsync.h"
#include "SqlBackend.h"
#include "SecureClientConnection.h"
#include "EncryptedItem.h"
#include "Globals.h"

#include <InternalError.h>
#include <FixedSizeNetworkPackage.h>

#include <cassert>
#include <QTcpSocket>
#include <QtEndian>
#include <QEventLoop>

SendSyncKeyAsync::SendSyncKeyAsync(QObject *parent)
:
	QThread(parent),
	stop(false),
	connection(nullptr)
{}

SendSyncKeyAsync::~SendSyncKeyAsync() {
	mutex.lock();
	stop = true;
	mutex.unlock();
	if (connection) connection->deleteLater();
	connection = nullptr;
}

void SendSyncKeyAsync::checkStop() {
	bool tmp;
	mutex.lock();
	tmp = stop;
	mutex.unlock();
	if (tmp) throw IERROR("STOP");
}

void SendSyncKeyAsync::run() {
	bool succ = true;
	try {
		createConnection();
		checkStop();
		sendSyncKey();
	} catch (const InternalError &e) {
		succ = false;
	} //TODO catch SqlNoResult
	emit done(succ);
	if (connection) connection->deleteLater();
	connection = nullptr;
}

void SendSyncKeyAsync::createConnection() {
	assert(connection == nullptr);
	connection = new SecureClientConnection(true);
	connect(connection, &SecureClientConnection::disconnected, [this]() {
			qDebug() << "Connection was unexpectedly closed";
			mutex.lock();
			stop = true;
			mutex.unlock();
			}
	);
	QEventLoop loop;
	connect(connection, &SecureClientConnection::cryptoEstablished, &loop, &QEventLoop::quit);
	connect(connection, &SecureClientConnection::disconnected, &loop, &QEventLoop::quit);
	loop.exec();
	//TODO check stop
}

void SendSyncKeyAsync::sendSyncKey() {
	connection->write("ADDSK");

	const QByteArray data = SqlBackend::getSyncPublicKey() + SqlBackend::getSyncSecretKey();

	QByteArray cipher, nonce;
	nonce.resize(crypto_secretbox_NONCEBYTES);
	cipher.resize(data.size() + crypto_secretbox_MACBYTES);
	randombytes_buf(reinterpret_cast<unsigned char*>(nonce.data()), nonce.size());
	if (crypto_secretbox_easy(
				reinterpret_cast<unsigned char*>(cipher.data()),
				reinterpret_cast<const unsigned char*>(data.constData()),
				data.size(),
				reinterpret_cast<const unsigned char*>(nonce.constData()),
				reinterpret_cast<const unsigned char*>(SqlBackend::getSyncKey().constData())
	) != 0) throw IERROR("Can't encrypt data");

	assert(16 >= crypto_generichash_BYTES_MIN && 16 <= crypto_generichash_BYTES_MAX);
	QByteArray id;
	id.resize(16);
	const QByteArray key = SqlBackend::getSyncKey();
	crypto_generichash(
			reinterpret_cast<unsigned char*>(id.data()),
			16,
			reinterpret_cast<const unsigned char*>(key.constData()),
			key.size(),
			nullptr,
			0
	);

	QByteArray tmp;
	tmp.resize(16);
	*reinterpret_cast<quint64*>(tmp.data()) = qToBigEndian<quint64>(cipher.size());
	*reinterpret_cast<quint64*>(tmp.data() + 8) = qToBigEndian<quint64>(nonce.size());
	QByteArray size;
	size.resize(8);
	*reinterpret_cast<quint64*>(size.data()) = qToBigEndian<quint64>(16 + 2 * 8 + cipher.size() + nonce.size());
	connection->write(size);
	connection->write(id);
	connection->write(tmp);
	connection->write(cipher);
	connection->write(nonce);
	FixedSizeNetworkPackage res(2);
	res.readBlocking(connection);
	if (res.getData() != QByteArray("OK")) throw IERROR("Server refused ADDUP");
}

void SendSyncKeyAsync::cancel() {
	mutex.lock();
	stop = true;
	mutex.unlock();
}
