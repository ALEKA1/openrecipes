/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef BACKEND_H
#define BACKEND_H

#include <QObject>
#include <QList>
#include <QQmlListProperty>
#include <memory>
#ifdef Q_OS_ANDROID
#include <QAndroidActivityResultReceiver>
#endif
#ifdef Q_OS_WIN
#include <QVersionNumber>
#endif

#include "Recipe.h"
#include "SqlBackend.h"

class SynchronizeAsync;
class SendSyncKeyAsync;
class RecvSyncKeyAsync;

#ifdef Q_OS_ANDROID
class Backend : public QObject, public QAndroidActivityResultReceiver {
#else
class Backend : public QObject {
#endif
	Q_OBJECT
	Q_PROPERTY(QQmlListProperty<Recipe> recipesList READ getRecipesList NOTIFY recipesListChanged)
	Q_PROPERTY(QString syncKeyHex READ getSyncKeyHex NOTIFY syncSettingsChanged);
	Q_PROPERTY(bool syncAvailable READ getSyncAvailable NOTIFY syncSettingsChanged);
	Q_PROPERTY(QString customSyncServerAddr READ getCustomSyncServerAddr WRITE setCustomSyncServerAddr NOTIFY syncSettingsChanged);
	Q_PROPERTY(int customSyncServerPort READ getCustomSyncServerPort WRITE setCustomSyncServerPort NOTIFY syncSettingsChanged);
	Q_PROPERTY(QString customSyncServerKeyHex READ getCustomSyncServerKeyHex WRITE setCustomSyncServerKeyHex NOTIFY syncSettingsChanged);
	Q_PROPERTY(bool useCustomSyncServer READ getUseCustomSyncServer WRITE setUseCustomSyncServer NOTIFY syncSettingsChanged);
	Q_PROPERTY(QString filter READ getFilter WRITE setFilter NOTIFY filterChanged);

	private:
		SynchronizeAsync *synchronizeAsync;
		SendSyncKeyAsync *sendSyncKeyAsync;
		RecvSyncKeyAsync *recvSyncKeyAsync;
		std::unique_ptr<SqlTransaction> transaction;
		QList<Recipe*> recipes;
		QString filter;

		void sortRecipes();

#ifdef Q_OS_ANDROID
		enum class AndroidRequestCodes : int {
			GetImage,
			GetQRCode
		};

		void handleActivityResult(int receiverRequestCode, int resultCode, const QAndroidJniObject &data) override;
#endif

#ifdef Q_OS_WIN
	private slots:
		void checkForUpdates();
#endif

	public:
		Backend();
		QQmlListProperty<Recipe> getRecipesList();
		Recipe* getRecipeAt(int pos);
		int getRecipesCount() const;
		QString getSyncKeyHex() const;
		bool getUseCustomSyncServer() const;
		QString getCustomSyncServerAddr() const;
		int getCustomSyncServerPort() const;
		QString getCustomSyncServerKeyHex() const;
		void setUseCustomSyncServer(bool use) const;
		void setCustomSyncServerAddr(const QString &addr) const;
		void setCustomSyncServerPort(int port) const;
		void setCustomSyncServerKeyHex(const QString &key) const;
		QString getFilter() const;
		void setFilter(const QString &filter);
		Q_INVOKABLE void setupSyncFromKey(const QString &key);
		Q_INVOKABLE void setupSync() const;
		Q_INVOKABLE void removeSyncSettings() const;
		bool getSyncAvailable() const;
		Q_INVOKABLE Recipe* getRecipe(const QByteArray &id);
		Q_INVOKABLE Recipe* getNewRecipe();
		Q_INVOKABLE int getRecipeListIndex(const QByteArray &id);
		Q_INVOKABLE bool deleteRecipe(Recipe *recipe);
		Q_INVOKABLE void synchronize();
		Q_INVOKABLE void stopSynchronizing();
		Q_INVOKABLE void startTransaction();
		Q_INVOKABLE void commitTransaction();
		Q_INVOKABLE void rollbackTransaction();
		Q_INVOKABLE void sendSyncKey();
#ifdef Q_OS_WIN
		Q_INVOKABLE void update(const QString &version);
#endif
#ifdef Q_OS_ANDROID
		Q_INVOKABLE void shareRecipe(Recipe *recipe);
		Q_INVOKABLE void requestImage();
		Q_INVOKABLE void requestQRCode();
#else
		Q_INVOKABLE bool importRecipeFromFile(const QString &path);
		Q_INVOKABLE bool saveRecipeToFile(Recipe *recipe, const QString &path);
#endif

	public slots:
		void onRecipesListChanged();
		void onRecipeChanged(const QByteArray &id);
		void onSyncSettingsChanged();

	signals:
		void recipesListChanged();
		void synchronizingDone(bool success);
		void sendSyncKeyDone(bool success);
		void recvSyncKeyDone(bool success);
		void openImage(const QUrl &src);
		void syncSettingsChanged();
		void filterChanged();
		void recipeImported(const QByteArray &id);

		/* Only used on windows. */
		void updateAvailable(const QString &version);
		void updateDownloadFailed();
};

#endif //BACKEND_H
