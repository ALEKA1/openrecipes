/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GLOBALS_H
#define GLOBALS_H

#include <QObject>

class QVersionNumber;

class Globals : public QObject {
	Q_OBJECT
	Q_PROPERTY(bool mobile READ isMobile CONSTANT)
	Q_PROPERTY(QString accentColor READ getAccentColor CONSTANT)
	Q_PROPERTY(QString minimumQtVersion READ getMinimumQtVersionStr CONSTANT)
	Q_PROPERTY(unsigned int recipesListImageHeight READ getRecipesListImageHeight CONSTANT)

	public:
		explicit Globals(QObject *parent = nullptr);
		static bool isMobile();
		static QString getAccentColor();
		static QString getMinimumQtVersionStr();
		static QVersionNumber getMinimumQtVersion();
		static unsigned int getRecipesListImageHeight();
		static QString getDefaultServerHostName();
		static quint16 getDefaultServerPort();
		static QByteArray getDefaultServerKey();
		static QByteArray getUpdateSigningKey();
};

#endif //GLOBALS_H
