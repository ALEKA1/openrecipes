/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef INGREDIENT_H
#define INGREDIENT_H

#include <QString>
#include <QObject>
#include <memory>

class Ingredient : public QObject {
	Q_OBJECT
	Q_PROPERTY(double multipliedCount READ getMultipliedCount NOTIFY multipliedCountChanged)
	Q_PROPERTY(double count READ getCount WRITE setCount NOTIFY dataChanged)
	Q_PROPERTY(QString unit READ getUnit WRITE setUnit NOTIFY dataChanged)
	Q_PROPERTY(QString article READ getArticle WRITE setArticle NOTIFY dataChanged)

	private:
		int id;
		double multiplier;
		
	public:
		Ingredient(int id, QObject *parent = nullptr);
		explicit Ingredient(const Ingredient &ingredient);
		int getId() const;
		void setMultiplier(double multiplier);
		double getCount() const;
		double getMultipliedCount() const;
		QString getUnit() const;
		QString getArticle() const;
		void setCount(double count);
		void setUnit(const QString &unit);
		void setArticle(const QString &article);

	public slots:
		void onIngredientChanged(int id);

	signals:
		void multipliedCountChanged();
		void dataChanged();
};

#endif //INGREDIENT_H
