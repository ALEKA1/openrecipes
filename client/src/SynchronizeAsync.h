/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SYNCHRONIZE_ASYNC_H
#define SYNCHRONIZE_ASYNC_H

#include <QThread>
#include <QMutex>
#include <vector>

class SecureClientConnection;

class EncryptedItem;

class SynchronizeAsync : public QThread {
	Q_OBJECT

	private:
		bool stop;
		QMutex mutex;
		SecureClientConnection *connection;

		void createConnection();
		std::vector<EncryptedItem> requestChanged();
		void checkStop();
		void checkProtocolVersion();
		quint64 requestStartupTime();
		std::vector<QByteArray> requestIds();
		void mitigateCollisions(const std::vector<EncryptedItem> &changedItemsRemote) const;
		void writeToDb(const std::vector<EncryptedItem> &changedItemsRemote) const;
		void sendChanged();
		void deleteLocalDeleted(const std::vector<QByteArray> &remoteIds);
		void endSync();

	public:
		explicit SynchronizeAsync(QObject *parent = nullptr);
		~SynchronizeAsync();
		void run() override;
		void cancel();

	signals:
		void done(bool success);
};

#endif //SYNCHRONIZE_ASYNC_H
