/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Backend.h"
#include "Recipe.h"
#include "QREncoder.h"
#include "Globals.h"
#include "SqlBackend.h"

#ifdef Q_OS_ANDROID
#include "Jni.h"
#endif

#include <config.h>

#include <QApplication>
#include <QQmlContext>
#include <QQmlApplicationEngine>
#include <QLocale>
#include <QTranslator>
#include <QLibraryInfo>
#include <QIcon>
#include <sodium.h>
#include <QDebug>

#ifdef Q_OS_ANDROID
	#include <QtAndroid>
	#include <QQuickStyle>
#else
	#include <QCommandLineParser>
#endif

int main(int argc, char **argv) {
	QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
	QApplication app(argc, argv);
	app.setWindowIcon(QIcon(":/img/icon.svg"));
	app.setApplicationName(QString("OpenRecipes%1").arg(config::namepostfix));
	app.setApplicationVersion(config::version);

	qmlRegisterUncreatableType<Recipe>("org.jschwab.recipes", 1, 0, "Recipe", "Can't create recipe");
	qmlRegisterUncreatableType<Ingredient>("org.jschwab.recipes", 1, 0, "Ingredient", "Can't create ingredient");
	qmlRegisterSingletonType<Globals>("org.jschwab.recipes", 1, 0, "Globals",
			[](QQmlEngine *engine, QJSEngine *scriptEngine) -> QObject* {
				Q_UNUSED(engine)
				Q_UNUSED(scriptEngine)
				return new Globals();
			}
	);

#ifndef Q_OS_ANDROID
	QCommandLineParser parser;
	parser.addHelpOption();
	parser.addVersionOption();
	parser.addPositionalArgument("file", QObject::tr("recipe to import"));
	parser.process(app);
	const QStringList args = parser.positionalArguments();
#endif

#ifdef Q_OS_ANDROID
	QQuickStyle::setStyle("Material");
#endif

	QTranslator qtTranslator, translator;
#ifdef Q_OS_ANDROID
	qtTranslator.load("qt_" + QLocale::system().name(), "assets:/translations");
#else
	qtTranslator.load("qt_" + QLocale::system().name(), QLibraryInfo::location(QLibraryInfo::TranslationsPath));
#endif
	translator.load(":/intl/client_" + QLocale::system().name());
	app.installTranslator(&qtTranslator);
	app.installTranslator(&translator);

	const QVersionNumber qtVersion = QVersionNumber::fromString(qVersion());
	if (qtVersion < Globals::getMinimumQtVersion()) {
		QQmlApplicationEngine engine;
		engine.load(QUrl("qrc:/qml/UnsupportedVersionWindow.qml"));
#ifdef Q_OS_ANDROID
		QtAndroid::hideSplashScreen();
#endif
		return app.exec();
	}

	if (sodium_init() == -1) {
		qCritical() << "Can't init libsodium!";
		return EXIT_FAILURE;
	}

	SqlBackend::init();
	Backend backend;

	QQmlApplicationEngine engine;
	engine.addImageProvider("QRCode", new QREncoder());
	engine.rootContext()->setContextProperty("backend", &backend);

#ifdef Q_OS_ANDROID
	engine.load(QUrl("qrc:/qml/MainWindowMobile.qml"));
	Jni::importBufferMutex.lock();
	for (const auto &xml : Jni::importBuffer) {
		try {
			Recipe *r = new Recipe(xml.toUtf8(), nullptr);
			emit Jni::getNotifier()->recipeImported(r->getId());
			delete r;
		} catch (const InternalError &e) {
			qWarning() << "Can't import buffered recipe";
		}
	}
	Jni::importBuffer.clear();
	Jni::importBufferMutex.unlock();
	QtAndroid::hideSplashScreen();
#else
	engine.load(QUrl("qrc:/qml/MainWindowDesktop.qml"));
	if (!args.empty()) backend.importRecipeFromFile(args.at(0));
#endif

	return app.exec();
}
