cd `dirname "${BASH_SOURCE[0]}"`
make distclean
export OPENRECIPES_ANDROID_SYSROOT=`pwd`/androidlibs/x86
export ANDROID_NDK_PLATFORM=android-27
~/Qt/5.11.1/android_x86/bin/qmake CONFIG+=debug
make clean
make -j5
mkdir client/android_x86
make install INSTALL_ROOT=android_x86
cd client
export TERM=xterm-color
~/Qt/5.11.1/android_x86/bin/androiddeployqt --output android_x86 --gradle --input android-libopenrecipes.so-deployment-settings.json
